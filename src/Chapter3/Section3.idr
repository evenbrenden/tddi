module Chapter3.Section3

import Data.Vect

transposeMat : {n : _} -> Vect m (Vect n element) -> Vect n (Vect m element)
transposeMat [] = replicate _ []
transposeMat (x :: xs) = let xsTrans = transposeMat xs in zipWith (::) x xsTrans

addMatrix : Num a => Vect n (Vect m a) -> Vect n (Vect m a) -> Vect n (Vect m a)
addMatrix = zipWith (zipWith (+))

dotProduct : Num a => Vect n a -> Vect n a -> a
dotProduct xs ys = sum $ zipWith (*) xs ys

multMatrixHelper : Num a => Vect m a -> Vect p (Vect m a) -> Vect p a
multMatrixHelper xs ys = map (dotProduct xs) ys

multMatrix : Num a => {p : _} -> Vect n (Vect m a) -> Vect m (Vect p a) -> Vect n (Vect p a)
multMatrix xs ys = map ((flip multMatrixHelper) ysTrans) xs
  where
    ysTrans : Vect p (Vect m a)
    ysTrans = transposeMat ys
