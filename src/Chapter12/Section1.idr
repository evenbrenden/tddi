module Chapter12.Section1

import Control.Monad.State

update : (stateType -> stateType) -> State stateType ()
update f = do
    s <- get
    put (f s)

increase : Nat -> State Nat ()
increase x = update (+x)

updateTest : (Nat, ())
updateTest = runState 89 (increase 5)

data Tree a =
    Empty
    | Node (Tree a) a (Tree a)

testTree : Tree String
testTree = Node (Node (Node Empty "Jim" Empty) "Fred"
                      (Node Empty "Sheila" Empty)) "Alice"
                (Node Empty "Bob" (Node Empty "Eve" Empty))

countEmpty : Tree a -> State Nat ()
countEmpty Empty = increase 1
countEmpty (Node left _ right) = do
    countEmpty left
    countEmpty right

countEmptyTest : Nat
countEmptyTest = execState 0 (countEmpty testTree)

countEmptyNode : Tree a -> State (Nat, Nat) ()
countEmptyNode Empty = update $ \(e, n) => (e + 1, n)
countEmptyNode (Node left _ right) = do
    countEmptyNode left
    update $ \(e, n) => (e, n + 1)
    countEmptyNode right

testCountEmptyNode : (Nat, Nat)
testCountEmptyNode = execState (0, 0) (countEmptyNode testTree)
