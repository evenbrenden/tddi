module Chapter9.Section1

import Decidable.Equality

data Elem : a -> List a -> Type where
    Here : Elem x (x :: xs)
    There : (later : Elem x xs) -> Elem x (y :: xs)

data Last : List a -> a -> Type where
    LastOne : Last [value] value
    LastCons : (prf : Last xs value) -> Last (x :: xs) value

notInEmpty : Last [] value -> Void
notInEmpty LastOne impossible
notInEmpty (LastCons prf) impossible

notInLast : (x = value -> Void) -> Last [x] value -> Void
notInLast contra LastOne = contra Refl
notInLast _ (LastCons LastOne) impossible
notInLast _ (LastCons (LastCons prf)) impossible

notInLastCons : (Last (x2 :: xs) value -> Void) -> Last (x1 :: (x2 :: xs)) value -> Void
notInLastCons contra (LastCons prf) = contra prf

isLast : DecEq a => (xs : List a) -> (value : a) -> Dec (Last xs value)
isLast [] value = No notInEmpty
isLast (x :: []) value =
    case decEq x value of
        Yes Refl => Yes LastOne
        No contra => No (notInLast contra)
isLast (x1 :: (x2 :: xs)) value =
    case isLast (x2 :: xs) value of
        (Yes prf) => Yes (LastCons prf)
        (No contra) => No (notInLastCons contra)
