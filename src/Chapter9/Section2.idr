module Chapter9.Section2

import Data.String
import Data.Vect
import Data.Vect.Elem
import Decidable.Equality

data WordState :
        (guessesLeft : Nat) ->
        (letters : Nat) ->
        Type
  where
    MkWordState :
        (word : String) ->
        (missing : Vect letters Char) ->
        WordState guessesLeft letters

data Finished : Type where
    Lost :
        (game : WordState 0 (S letters)) ->
        Finished
    Won :
        (game : WordState (S guesses) 0) ->
        Finished

data ValidInput :
    List Char ->
    Type
  where
    Letter :
        (c : Char) ->
        ValidInput [c]

isValidNil :
    ValidInput [] ->
    Void
isValidNil (Letter _) impossible

isValidTwo :
    ValidInput (x1 :: x2 :: xs) ->
    Void
isValidTwo (Letter _) impossible

isValidInput :
    (cs : List Char) ->
    Dec (ValidInput cs)
isValidInput [] = No isValidNil
isValidInput (x :: []) = Yes (Letter x)
isValidInput (x1 :: (x2 :: xs)) = No isValidTwo

isValidString :
    (s : String) ->
    Dec (ValidInput (unpack s))
isValidString s = isValidInput (unpack s)

readGuess : IO Char
readGuess = do
    (_ ** Letter letter) <- go
    pure letter
  where
    go : IO (x ** ValidInput x)
    go = do
        putStr "Guess a letter: "
        input <- getLine
        case isValidString (toUpper input) of
            Yes prf => pure (_ ** prf)
            No contra => do
                putStrLn "I said a letter"
                go

removeElem :
    {n : _} ->
    (value : a) ->
    (xs : Vect (S n) a) ->
    {auto prf : Elem value xs} ->
    Vect n a
removeElem value (value :: ys) {prf = Here} = ys
removeElem {n = Z} value (y :: []) {prf = There later} = absurd later
removeElem {n = (S k)} value (y :: ys) {prf = There later} = y :: removeElem value ys

processGuess :
    {letters: _} ->
    (letter : Char) ->
    WordState (S guesses) (S letters) ->
    Either (WordState guesses (S letters)) (WordState (S guesses) letters)
processGuess letter (MkWordState word missing) =
    case isElem letter missing of
        Yes prf => Right (MkWordState word (removeElem letter missing))
        No contra => Left (MkWordState word missing)

game :
    {guesses : _} ->
    {letters : _} ->
    WordState (S guesses) (S letters) ->
    IO Finished
game {guesses} {letters} state = do
    letter <- readGuess
    case processGuess letter state of
        Left newState => do
            putStrLn "Wrong!"
            case guesses of
                Z => pure (Lost newState)
                S k => game newState
        Right newState => do
            putStrLn "Right!"
            case letters of
                Z => pure (Won newState)
                S k => game newState

run : IO ()
run = do
    result <- game {guesses=2} (MkWordState "Test" ['T', 'E', 'S'])
    case result of
        Lost (MkWordState word missing) =>
            putStrLn ("You lose! The word is " ++ word)
        Won (MkWordState word _) =>
            putStrLn ("You win! The word is " ++ word)
