module Chapter5.Section3

import Data.String
import Data.Vect
import System.File

readToBlank : IO (List String)
readToBlank = do
    input <- getLine
    case input of
         "" => pure []
         _ => do
             next <- readToBlank
             pure $ input :: next

readAndSave : IO ()
readAndSave = do
    input <- readToBlank
    putStr "Filename: "
    filename <- getLine
    let contents = unlines input
    Right () <- writeFile filename contents | Left err => putStrLn (show err)
    pure ()

readVectFile : (filename : String) -> IO (n ** Vect n String)
readVectFile filename = do
    Right handle <- openFile filename Read | Left err => putStrLn (show err) >> pure (_ ** [])
    result <- go handle
    closeFile handle
    pure result
  where
    go : (handle : File) -> IO (n ** Vect n String)
    go handle = do
        Right x <- fGetLine handle | Left err => putStrLn (show err) >> pure (_ ** [])
        isEOF <- fEOF handle
        if (isEOF)
            then do
                pure (_ ** [])
            else do
                (_ ** xs) <- go handle
                pure (_ ** x :: xs)
