module Chapter5.Section1

printLonger : IO ()
printLonger = do
        x <- getLine
        y <- getLine
        print $ max (length x) (length y)

printLonger' : IO ()
printLonger' = getLine >>= \x => getLine >>= \y => print $ max (length x) (length y)
