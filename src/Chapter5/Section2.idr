module Chapter5.Section2

import System

guess : (target : Nat) -> (guesses : Nat) -> IO ()
guess t c = do
        putStrLn $ "Guess a number (" ++ show c ++ " guesses)"
        g <- getLine
        if all isDigit (unpack g)
            then if cast g == t
                    then putStrLn "Correct!"
                    else putStrLn "Wrong!" >> guess t (c + 1)
            else putStrLn "Invalid input" >> guess t c

guessRandom : IO ()
guessRandom = time >>= (flip guess) 0 . integerToNat

rapl : (prompt : String) -> (f : String -> String) -> IO ()
rapl prompt f = do
    putStr prompt
    input <- getLine
    putStr (f input)
    rapl prompt f

raplWith : (state : a) -> (prompt : String) -> (onInput : a -> String -> Maybe (String, a)) -> IO ()
raplWith state prompt onInput = do
    putStr prompt
    input <- getLine
    case onInput state input of
        Just (output, newState) => do
            putStr output
            raplWith newState prompt onInput
        Nothing => raplWith state prompt onInput
