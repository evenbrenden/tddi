module Chapter13.Section2

import Data.Vect

%default total

data StackCmd : Type -> Nat -> Nat -> Type where
    Push : Integer -> StackCmd () height (S height)
    Pop : StackCmd Integer (S height) height
    Top : StackCmd Integer (S height) (S height)

    GetStr : StackCmd String height height
    PutStr : String -> StackCmd () height height

    Pure : ty -> StackCmd ty height height
    (>>=) : StackCmd a height1 height2 -> (a -> StackCmd b height2 height3) -> StackCmd b height1 height3
    (>>) : StackCmd a height1 height2 -> StackCmd b height2 height3 -> StackCmd b height1 height3

runStack : (stk : Vect inHeight Integer) -> StackCmd ty inHeight outHeight -> IO (ty, Vect outHeight Integer)
runStack stk (Push val) = pure ((), val :: stk)
runStack (val :: stk) Pop = pure (val, stk)
runStack (val :: stk) Top = pure (val, val :: stk)
runStack stk GetStr = do
    x <- getLine
    pure (x, stk)
runStack stk (PutStr x) = do
    putStr x
    pure ((), stk)
runStack stk (Pure x) = pure (x, stk)
runStack stk (cmd >>= next) = do
    (cmdRes, newStk) <- runStack stk cmd
    runStack newStk (next cmdRes)
runStack stk (cmd >> next) = do
    (cmdRes, newStk) <- runStack stk cmd
    runStack newStk next

data StackIO : Nat -> Type where
    Do : StackCmd a height1 height2 -> (a -> Inf (StackIO height2)) -> StackIO height1
    Seq : StackCmd a height1 height2 -> Inf (StackIO height2) -> StackIO height1

namespace StackDo
    export
    (>>=) : StackCmd a height1 height2 -> (a -> Inf (StackIO height2)) -> StackIO height1
    (>>=) = Do

    export
    (>>) : StackCmd a height1 height2 -> Inf (StackIO height2) -> StackIO height1
    (>>) = Seq

data Fuel = Dry | More (Lazy Fuel)

partial
forever : Fuel
forever = More forever

run : Fuel -> Vect height Integer -> StackIO height -> IO ()
run (More fuel) stk (Do c f) = do
    (res, newStk) <- runStack stk c
    run fuel newStk (f res)
run (More fuel) stk (Seq c d) = do
    (res, newStk) <- runStack stk c
    run fuel newStk d
run Dry stk p = pure ()

data StkInput =
    Number Integer
    | Add
    | Subtract
    | Multiply
    | Negate
    | Discard
    | Duplicate

strToInput : String -> Maybe StkInput
strToInput "" = Nothing
strToInput "add" = Just Add
strToInput "subtract" = Just Subtract
strToInput "multiply" = Just Multiply
strToInput "negate" = Just Negate
strToInput "discard" = Just Discard
strToInput "duplicate" = Just Duplicate
strToInput x =
    if all isDigit (unpack x)
        then Just (Number (cast x))
        else Nothing

BinaryOperation : Type
BinaryOperation = Integer -> Integer -> Integer

doBinaryOperation : BinaryOperation -> StackCmd () (S (S height)) (S height)
doBinaryOperation op = do
    val1 <- Pop
    val2 <- Pop
    Push (op val1 val2)

UnaryOperation : Type
UnaryOperation = Integer -> Integer

doUnaryOperation : UnaryOperation -> StackCmd () (S height) (S height)
doUnaryOperation op = do
    val <- Pop
    Push (op val)

mutual
    tryBinaryOperation : {height : _} -> BinaryOperation -> StackIO height
    tryBinaryOperation {height = (S (S h))} op = do
        doBinaryOperation op
        result <- Top
        PutStr (show result ++ "\n")
        stackCalc
    tryBinaryOperation op = do
        PutStr "Fewer than two items on the stack\n"
        stackCalc

    tryUnaryOperation : {height : _} -> UnaryOperation -> StackIO height
    tryUnaryOperation {height = (S h)} op = do
        doUnaryOperation op
        result <- Top
        PutStr (show result ++ "\n")
        stackCalc
    tryUnaryOperation op = do
        PutStr "No items on the stack\n"
        stackCalc

    tryDiscard : {height : _} -> StackIO height
    tryDiscard {height = (S h)} = do
        value <- Pop
        PutStr ("Discarded " ++ show value ++ "\n")
        stackCalc
    tryDiscard = do
        PutStr "No items on the stack\n"
        stackCalc

    tryDuplicate : {height : _} -> StackIO height
    tryDuplicate {height = (S h)} = do
        value <- Pop
        Push value
        Push value
        PutStr ("Duplicated " ++ show value ++ "\n")
        stackCalc
    tryDuplicate = do
        PutStr "No items on the stack\n"
        stackCalc

    stackCalc : {height : _} -> StackIO height
    stackCalc = do
        PutStr "> "
        input <- GetStr
        case strToInput input of
            Nothing => do
                PutStr "Invalid input\n"
                stackCalc
            Just (Number x) => do
                Push x
                stackCalc
            Just Add => tryBinaryOperation (+)
            Just Subtract => tryBinaryOperation (-)
            Just Multiply => tryBinaryOperation (*)
            Just Negate => tryUnaryOperation (0-)
            Just Discard => tryDiscard
            Just Duplicate => tryDuplicate

partial
export
runCalc : IO ()
runCalc = run forever [] stackCalc
