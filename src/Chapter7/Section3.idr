module Chapter7.Section3

data Expr num =
    Val num
    | Add (Expr num) (Expr num)
    | Sub (Expr num) (Expr num)
    | Mul (Expr num) (Expr num)
    | Div (Expr num) (Expr num)
    | Abs (Expr num)

Functor Expr where
    map f (Val x) = Val (f x)
    map f (Add x y) = Add (map f x) (map f y)
    map f (Sub x y) = Sub (map f x) (map f y)
    map f (Mul x y) = Mul (map f x) (map f y)
    map f (Div x y) = Div (map f x) (map f y)
    map f (Abs x) = Abs (map f x)

data Vekt : Nat -> Type -> Type where
    Nill : Vekt Z a
    Kons : (x : a) -> (xs : Vekt k a) -> Vekt (S k) a

Eq e => Eq (Vekt n e) where
    (==) Nill Nill = True
    (==) (Kons x xs) (Kons y ys) = x == y && xs == ys
    (==) _ _ = False

Foldable (Vekt n) where
    foldr func acc Nill = acc
    foldr func acc (Kons x xs) = func x (foldr func acc xs)
