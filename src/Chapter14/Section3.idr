module Chapter14.Section3

import Decidable.Equality
import Data.List
import Data.String
import Data.Vect
import Data.Vect.Elem

%default total

data GameState : Type where
    NotRunning : GameState
    Running : (guessesLeft : Nat) -> (lettersLeft : Nat) -> GameState

MaxGuesses : Nat
MaxGuesses = 6

uniqueLetters : String -> List Char
uniqueLetters str = nub . map toUpper . unpack $ str

data GuessResult = Correct | Incorrect

data GameCmd : (ty : Type) -> GameState -> (ty -> GameState) -> Type where
    NewGame : (word : String) -> GameCmd () NotRunning (const (Running MaxGuesses (length (uniqueLetters word))))
    Won : GameCmd () (Running (S guessesLeft) 0) (const NotRunning)
    Lost : GameCmd () (Running 0 (S guessesLeft)) (const NotRunning)
    Guess : (c : Char) -> GameCmd GuessResult
                            (Running (S guessesLeft) (S lettersLeft))
                                (\result => case result of
                                    Correct => Running (S guessesLeft) lettersLeft
                                    Incorrect => Running guessesLeft (S lettersLeft))

    ShowState : GameCmd () state (const state)
    Message : String -> GameCmd () state (const state)
    ReadGuess : GameCmd Char state (const state)

    Pure : (result : ty) -> GameCmd ty (state_fn result) state_fn
    (>>=) : GameCmd a state1 state2_fn -> ((result : a) -> GameCmd b (state2_fn result) state3_fn) -> GameCmd b state1 state3_fn
    (>>) : GameCmd () state1 state2_fn -> GameCmd b (state2_fn ()) state3_fn -> GameCmd b state1 state3_fn

namespace Loop
    public export
    data GameLoop : (ty : Type) -> GameState -> (ty -> GameState) -> Type where
        (>>=) : GameCmd a state1 state2_fn -> ((result : a) -> Inf (GameLoop b (state2_fn result) state3_fn)) -> GameLoop b state1 state3_fn
        (>>) : GameCmd () state1 state2_fn -> Inf (GameLoop b (state2_fn ()) state3_fn) -> GameLoop b state1 state3_fn
        Exit : GameLoop () NotRunning (const NotRunning)

    gameLoop : {lettersLeft : _} -> {guessesLeft : _} -> GameLoop () (Running (S guessesLeft) (S lettersLeft)) (const NotRunning)
    gameLoop {guessesLeft} {lettersLeft} = do
        ShowState
        input <- ReadGuess
        guess <- Guess input
        case guess of
            Correct => case lettersLeft of
                Z => do
                    Won
                    ShowState
                    Exit
                S k => do
                    Message "Correct"
                    gameLoop
            Incorrect => case guessesLeft of
                Z => do
                    Lost
                    ShowState
                    Exit
                S k => do
                    Message "Incorrect"
                    gameLoop

    export
    hangman : GameLoop () NotRunning (const NotRunning)
    hangman = do
        NewGame "testing"
        gameLoop

data Game : GameState -> Type where
    GameStart : Game NotRunning
    GameWon : (word : String) -> Game NotRunning
    GameLost : (word : String) -> Game NotRunning
    InProgress : {lettersLeft : _} -> (word : String) -> (guessesLeft : Nat) -> (missing : Vect lettersLeft Char) -> Game (Running guessesLeft lettersLeft)

Show (Game gameState) where
    show GameStart = "Starting"
    show (GameWon word) = "Game won! The word was " ++ word
    show (GameLost word) = "Game lost! The word was " ++ word
    show (InProgress word guessesLeft missing) = "\n" ++ pack (map hideMissing (unpack word)) ++ "\n" ++ show guessesLeft ++ " guesses left"
      where
        hideMissing : Char -> Char
        hideMissing c = if c `elem` missing then '-' else c

data Fuel = Dry | More (Lazy Fuel)

data GameTank : (ty : Type) -> (ty -> GameState) -> Type where
    HasFuel : (result : ty) -> Game (outstate_fn result) -> GameTank ty outstate_fn
    OutOfFuel : GameTank ty outstate_fn

hasFuel : (result : ty) -> Game (outstate_fn result) -> IO (GameTank ty outstate_fn)
hasFuel result state = pure (HasFuel result state)

removeElem : {n : _} -> (value : a) -> (xs : Vect (S n) a) -> {auto prf : Elem value xs} -> Vect n a
removeElem value (value :: ys) {prf = Here} = ys
removeElem {n = Z} value (y :: []) {prf = There later} = absurd later
removeElem {n = (S k)} value (y :: ys) {prf = There later} = y :: removeElem value ys

runCmd : Fuel -> Game instate -> GameCmd ty instate outstate_fn -> IO (GameTank ty outstate_fn)
runCmd fuel state (NewGame word) = hasFuel () (InProgress (toUpper word) _ (fromList (uniqueLetters word)))
runCmd fuel (InProgress word _ missing) Won = hasFuel () (GameWon word)
runCmd fuel (InProgress word _ missing) Lost = hasFuel () (GameLost word)
runCmd fuel (InProgress word _ missing) (Guess c) =
    case isElem c missing of
        Yes prf => hasFuel Correct (InProgress word _ (removeElem c missing))
        No contra => hasFuel Incorrect (InProgress word _ missing)
runCmd fuel state ShowState = do
    printLn state
    hasFuel () state
runCmd fuel state (Message str) = do
    putStrLn str
    hasFuel () state
runCmd (More fuel) state ReadGuess = do
    putStr "Guess: "
    input <- getLine
    case unpack input of
        [c] =>
            if isAlpha c
                then hasFuel (toUpper c) state
                else do
                    putStrLn "Invalid input"
                    runCmd fuel state ReadGuess
        _ => do
            putStrLn "Invalid input"
            runCmd fuel state ReadGuess
runCmd fuel state (Pure result) = hasFuel result state
runCmd fuel state (cmd >>= next) = do
    HasFuel cmdRes newState <- runCmd fuel state cmd
        | OutOfFuel => pure OutOfFuel
    runCmd fuel newState (next cmdRes)
runCmd fuel state (cmd >> next) = do
    HasFuel () newState <- runCmd fuel state cmd
        | OutOfFuel => pure OutOfFuel
    runCmd fuel newState next
runCmd Dry _ _ = pure OutOfFuel

run : Fuel -> Game instate -> GameLoop ty instate outstate_fn -> IO (GameTank ty outstate_fn)
run (More fuel) state (cmd >>= next) = do
    HasFuel cmdRes newState <- runCmd fuel state cmd
        | OutOfFuel => pure OutOfFuel
    run fuel newState (next cmdRes)
run (More fuel) state (cmd >> next) = do
    HasFuel () newState <- runCmd fuel state cmd
        | OutOfFuel => pure OutOfFuel
    run fuel newState next
run (More fuel) state Exit = pure (HasFuel () state)
run Dry _ _ = pure OutOfFuel

partial
forever : Fuel
forever = More forever

partial
runHangman : IO ()
runHangman = do
    ignore $ run forever GameStart hangman
    pure ()
