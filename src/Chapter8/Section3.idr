module Chapter8.Section3

import Decidable.Equality

data Vekt : Nat -> Type -> Type where
    Nill : Vekt Z a
    Kons : (x : a) -> (xs : Vekt k a) -> Vekt (S k) a

total
headUnequal : DecEq a => {xs : Vekt n a} -> {ys : Vekt n a} -> (contra : (x = y) -> Void) -> (Kons x xs = Kons y ys) -> Void
headUnequal contra Refl = contra Refl

total
tailUnequal : DecEq a => {xs : Vekt n a} -> {ys : Vekt n a} -> (contra : (xs = ys) -> Void) -> (Kons x xs = Kons y ys) -> Void
tailUnequal contra Refl = contra Refl

headAndTailEqual : x = y -> xs = ys -> Kons x xs = Kons y ys
headAndTailEqual Refl Refl = Refl

DecEq a => DecEq (Vekt n a) where
    decEq Nill Nill = Yes Refl
    decEq (Kons x xs) (Kons y ys) =
        case decEq x y of
             (Yes prfHead) =>
                 case decEq xs ys of
                    (Yes prfTail) => Yes (headAndTailEqual prfHead prfTail)
                    (No contra) => No (tailUnequal contra)
             (No contra) => No (headUnequal contra)
