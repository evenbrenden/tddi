module Chapter8.Section1

checkEqNat : (num1 : Nat) -> (num2 : Nat) -> Maybe (num1 = num2)
checkEqNat Z Z = Just Refl
checkEqNat Z (S k) = Nothing
checkEqNat (S k) Z = Nothing
checkEqNat (S k) (S j) = do
    prf <- checkEqNat k j
    pure (cong S prf)

same_cons : {x : a} -> {xs : List a} -> {ys : List a} -> xs = ys -> x :: xs = x :: ys
same_cons {x = e} prf = cong ((::) e) prf

same_lists : {xs : List a} -> {ys : List a} -> x = y -> xs = ys -> x :: xs = y :: ys
-- This says that if x = y can proven (constructed) and xs == ys can be proven (constructed) then so can x :: xs = y :: ys
same_lists Refl Refl = Refl

data TwoEq : a -> b -> Type where
    Rofl : TwoEq x x

sameS : (k : Nat) -> (j : Nat) -> (eq : TwoEq k j) -> TwoEq (S k) (S j)
sameS k k Rofl = Rofl

data ThreeEq : a -> b -> c -> Type where
    Rolf : ThreeEq x x x

-- This means that if x = y = z then S x = S y = S z
allSameS : (x, y, z : Nat) -> ThreeEq x y z -> ThreeEq (S x) (S y) (S z)
allSameS x x x Rolf = Rolf

-- https://docs.idris-lang.org/en/latest/proofs/patterns.html
plusXeroRightNeutral : (m : Nat) -> m = plus m 0
plusXeroRightNeutral 0 = Refl
plusXeroRightNeutral (S k) =
    let rec = plusXeroRightNeutral k
    in rewrite sym rec in Refl

plusSukkRightSukk : (k : Nat) -> (m : Nat) -> S (plus m k) = plus m (S k)
plusSukkRightSukk k 0 = Refl
plusSukkRightSukk k (S j) = rewrite plusSukkRightSukk k j in Refl
