module Chapter8.Section2

import Data.Vect

-- https://docs.idris-lang.org/en/latest/proofs/patterns.html
plusXeroRightNeutral : (m : Nat) -> m = plus m 0
plusXeroRightNeutral 0 = Refl
plusXeroRightNeutral (S k) =
    let rec = plusXeroRightNeutral k
    in rewrite sym rec in Refl

plusSukkRightSukk : (k : Nat) -> (m : Nat) -> S (plus m k) = plus m (S k)
plusSukkRightSukk k 0 = Refl
plusSukkRightSukk k (S j) = rewrite plusSukkRightSukk k j in Refl

total
myPlusCommutes : (n : Nat) -> (m : Nat) -> n + m = m + n
myPlusCommutes 0 m = plusXeroRightNeutral m
myPlusCommutes (S k) m = rewrite myPlusCommutes k m in plusSukkRightSukk k m

reverseProof_nil : Vect m e -> Vect (plus m 0) e
reverseProof_nil {m} xs = rewrite plusZeroRightNeutral m in xs

reverseProof_xs : Vect (S (m + k)) e -> Vect (plus m (S k)) e
reverseProof_xs {m} {k} xs = rewrite sym (plusSuccRightSucc m k) in xs

myReverse : Vect n a -> Vect n a
myReverse xs = reverse' [] xs
  where
    reverse' : Vect k a -> Vect m a -> Vect (k + m) a
    reverse' acc [] = reverseProof_nil acc
    reverse' acc (x :: xs) = reverseProof_xs (reverse' (x :: acc) xs)
