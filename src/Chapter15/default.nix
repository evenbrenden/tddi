{ pkgs }:

pkgs.stdenv.mkDerivation {
  pname = "adder";
  version = "0.1.0";
  src = ./.;
  buildInputs = with pkgs; [ idris ];
  buildPhase = ''
    mkdir -p $out/bin
    idris -o adder adder.idr
  '';
  installPhase = ''
    cp -r adder $out/bin
  '';
}
