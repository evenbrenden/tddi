# Chapter 15

From what I gather, Idris 2 doesn't have channels at the moment ([idris2-buffered-channels](https://github.com/CodingCellist/idris2-buffered-channels) looks promising though). So this is some Idris 1 code from chapter 15 (with some changes that I thought made sense).
