module Chapter2.Section4

import Data.List
import Data.String
import System.REPL

sss : (String, String, String)
sss = ("A", "B", "C")

ls : List String
ls = ["A", "B", "C"]

csc : ((Char, String), Char)
csc = (('A', "B"), 'C')

palindrome : Nat -> String -> Bool
palindrome n s = length s > n && toLower s == toLower (reverse s)

counts : String -> (Nat, Nat)
counts s = (length (words s), length s)

top_ten : Ord a => List a -> List a
top_ten l = take 10 $ reverse (sort l)

over_length : Nat -> List String -> Nat
over_length n ls = length $ filter (\s => length s > n) ls

palindromeIO : IO ()
palindromeIO = repl "Enter a string: " (show . palindrome 0)

countsIO : IO ()
countsIO = repl "Enter a string: " (show . counts)
