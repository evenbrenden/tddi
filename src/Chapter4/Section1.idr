module Chapter4.Section1

data BSTree : Type -> Type where
    Empti : Ord a => BSTree a
    Noad : Ord a => (left : BSTree a) -> (val : a) -> (right : BSTree a) -> BSTree a

insurt : a -> BSTree a -> BSTree a
insurt x Empti = Noad Empti x Empti
insurt x orig@(Noad left val right) =
    case compare x val of
        LT => Noad (insurt x left) val right
        EQ => orig
        GT => Noad left val (insurt x right)

listToTree : Ord a => List a -> BSTree a
listToTree [] = Empti
listToTree (x :: xs) = insurt x (listToTree xs)

treeToList : BSTree a -> List a
treeToList Empti = []
treeToList (Noad left val right) = treeToList left ++ [val] ++ treeToList right

data Expr =
      Add Expr Expr
    | Sub Expr Expr
    | Mult Expr Expr
    | Val Int

evaluate : Expr -> Int
evaluate (Add x y) = evaluate x + evaluate y
evaluate (Sub x y) = evaluate x - evaluate y
evaluate (Mult x y) = evaluate x * evaluate y
evaluate (Val x) = x

maxMaybe : Ord a => Maybe a -> Maybe a -> Maybe a
maxMaybe Nothing Nothing = Nothing
maxMaybe Nothing (Just x) = Just x
maxMaybe (Just x) Nothing = Just x
maxMaybe (Just x) (Just y) = case x > y of
                                  False => Just y
                                  True => Just x

data Shape =
    Triangle Double Double
    | Rectangle Double Double
    | Circle Double

area : Shape -> Double
area (Triangle base height) = 0.5 * base * height
area (Rectangle length height) = length * height
area (Circle radius) = pi * radius * radius

data Picture =
    Primitive Shape
    | Combine Picture Picture
    | Rotate Double Picture
    | Translate Double Double Picture

pictureArea : Picture -> Double
pictureArea (Primitive shape) = area shape
pictureArea (Combine pic pic1) = pictureArea pic + pictureArea pic1
pictureArea (Rotate x pic) = pictureArea pic
pictureArea (Translate x y pic) = pictureArea pic

biggestTriangle : Picture -> Maybe Double
biggestTriangle (Primitive triangle@(Triangle x y)) = Just $ area triangle
biggestTriangle (Primitive (Rectangle _ _)) = Nothing
biggestTriangle (Primitive (Circle _)) = Nothing
biggestTriangle (Combine x y) = maxMaybe (biggestTriangle x) (biggestTriangle y)
biggestTriangle (Rotate _ x) = biggestTriangle x
biggestTriangle (Translate _ _ x) = biggestTriangle x
