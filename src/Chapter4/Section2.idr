module Chapter4.Section2

import Data.Vect

data PowerSource = Petrol | Pedal | Electricity

data Vehicle : PowerSource -> Type where
    Bicycle : Vehicle Pedal
    Car : (fuel : Nat) -> Vehicle Petrol
    Bus : (fuel : Nat) -> Vehicle Petrol
    Unicycle : Vehicle Pedal
    Motorcycle : (fuel : Nat) -> Vehicle Petrol
    ElectricCar : (charge : Nat) -> Vehicle Electricity
    Tram : (charge : Nat) -> Vehicle Electricity

wheels : Vehicle povver -> Nat
wheels Bicycle = 2
wheels (Car fuel) = 4
wheels (Bus fuel) = 4
wheels Unicycle = 1
wheels (Motorcycle fuel) = 2
wheels (ElectricCar charge) = 4
wheels (Tram charge) = 0

refuel : Vehicle Petrol -> Vehicle Petrol
refuel (Car fuel) = Car 100
refuel (Bus fuel) = Bus 200
refuel (Motorcycle fuel) = Motorcycle 50

vectTake : {a : Type} -> {n : Nat} -> (m : Nat) -> Vect (m + n) a -> Vect m a
vectTake Z _ = []
vectTake (S k) (x :: xs) = x :: vectTake k xs

sumEntries : Num a => {n : _} -> (pos : Integer) -> Vect n a -> Vect n a -> Maybe a
sumEntries {n} pos xs ys =
    case integerToFin pos n of
        Just fos => Just $ index fos xs + index fos ys
        Nothing => Nothing
