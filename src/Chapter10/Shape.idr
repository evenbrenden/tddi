module Chapter10.Shape

export
data Shape =
    Triangle Double Double
    | Rectangle Double Double
    | Circle Double

export
triangle : Double -> Double -> Shape
triangle = Triangle

export
rectangle : Double -> Double -> Shape
rectangle = Rectangle

export
circle : Double -> Shape
circle = Circle

public export
data AreaView : Shape -> Type where
    STriangle : {base, height : _} -> AreaView (triangle base height)
    SRectangle : {length, height : _} -> AreaView (rectangle length height)
    SCircle : {radius : _} -> AreaView (circle radius)

export
areaView : (shape : Shape) -> AreaView shape
areaView (Triangle base height) = STriangle
areaView (Rectangle length height) = SRectangle
areaView (Circle radius) = SCircle
