module Chapter10.Section3

import Chapter10.DataStore
import Chapter10.Shape

testStore : DataStore (SString .+. SString .+. SInt)
testStore =
    addToStore ("Mercury", "Mariner 10", 1974) $
    addToStore ("Venus", "Venera", 1961) $
    addToStore ("Uranus", "Voyager 2", 1986) $
    addToStore ("Pluto", "New Horizons", 2015) $
    Chapter10.DataStore.empty

listItems : DataStore schema -> List (SchemaType schema)
listItems input with (storeView input)
    listItems Chapter10.DataStore.empty | SNil = []
    listItems (addToStore value store) | (SAdd _ _ rec) = value :: listItems store | rec

filterKeys : (test : SchemaType val_schema -> Bool) -> DataStore (SString .+. val_schema) -> List String
filterKeys test input with (storeView input)
    filterKeys test Chapter10.DataStore.empty | SNil = []
    filterKeys test (addToStore (key, value) store) | (SAdd _ _ rec) =
        if test value
             then key :: filterKeys test store | rec
             else filterKeys test store | rec

getValues : DataStore (SString .+. val_schema) -> List (SchemaType val_schema)
getValues input with (storeView input)
    getValues Chapter10.DataStore.empty | SNil = []
    getValues (addToStore entry store) | (SAdd entry store rec) = snd entry :: getValues store
area : Shape -> Double
area s with (areaView s)
    area (triangle base height) | STriangle = (base * height) / 2
    area (rectangle width height) | SRectangle = width * height
    area (circle radius) | SCircle = pi * radius * radius
