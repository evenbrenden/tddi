module Chapter10.Section2

import Data.Nat.Views
import Data.List.Views
import Data.List.Views.Extra

-- "This doesn't yet get past the totality checker, however, because it doesn't know about looking inside pairs."
-- https://idris2.readthedocs.io/en/latest/typedd/typedd.html#chapter-10
equalSuffix : Eq a => List a -> List a -> List a
equalSuffix xs ys with (snocList xs, snocList ys)
    equalSuffix _ _ | (Snoc x xs xsrec, Snoc y ys ysrec) =
        if x == y
            then (equalSuffix _ _ | (xsrec, ysrec)) ++ [x]
            else []
    equalSuffix _ _  | (Empty, s) = []
    equalSuffix _ _  | (s, Empty) = []

-- https://github.com/idris-lang/Idris2/issues/2834
-- mergeSort : Ord a => {n : _} -> Vect n a -> Vect n a
-- mergeSort input with (splitRec input)
--     mergeSort [] | SplitRecNil = []
--     mergeSort [x] | SplitRecOne = [x]
--     mergeSort (xs ++ ys) | (SplitRecPair {xs} {ys} lrec rrec) =
--         merge (mergeSort xs | lrec) (mergeSort ys | rrec)

total
toBinary : (n : Nat) -> String
toBinary n with (halfRec n)
    toBinary 0 | HalfRecZ = ""
    toBinary (k + k) | (HalfRecEven k rec) = (toBinary k | rec) ++ "0"
    toBinary (S (k + k)) | (HalfRecOdd k rec) = (toBinary k | rec) ++ "1"

total
palindrome : List Char -> Bool
palindrome xs with (vList xs)
  palindrome [] | VNil = True
  palindrome [x] | VOne = True
  palindrome (x :: (xs ++ [y])) | (VCons rec) =
      if x == y
          then palindrome xs | rec
          else False
