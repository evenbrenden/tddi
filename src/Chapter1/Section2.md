# Chapter 1 Section 2

> Input type: Vect n elem
> Output type: Vect n elem

`map`, `reverse`.

> Input type: Vect n elem
> Output type: Vect (n \* 2) elem

`duplicate`.

> Input type: Vect (1 + n) elem
> Output type: Vect n elem

`tail`, `init`.

> Input types: Bounded n, Vect n elem
> Output type: elem

`index`.
