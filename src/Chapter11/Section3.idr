module Chapter11.Section3

import Chapter11.Werds
import Data.Bits
import Data.Primitives.Views
import Data.String
import System
import System.File.ReadWrite

%default total

data Command : Type -> Type where
    PutStr : String -> Command ()
    GetLine : Command String
    ReadFile : String -> Command (Either FileError String)
    WriteFile : String -> String -> Command (Either FileError ())
    Pure : ty -> Command ty
    Bind : Command a -> (a -> Command b) -> Command b

namespace CommandDo
    export
    (>>=) : Command a -> (a -> Command b) -> Command b
    (>>=) = Bind

    export
    (>>) : Command () -> Command b -> Command b
    ma >> mb = Bind ma (\_ => mb)

partial -- Because readFile is partial
runCommand : Command a -> IO a
runCommand (PutStr x) = putStr x
runCommand GetLine = getLine
runCommand (ReadFile filename) = readFile filename
runCommand (WriteFile filename content) = writeFile filename content
runCommand (Pure val) = pure val
runCommand (Bind c f) = do
    res <- runCommand c
    runCommand (f res)

data ConsoleIO : Type -> Type where
    Quit : a -> ConsoleIO a
    Do : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b
    Seq : Command () -> Inf (ConsoleIO a) -> ConsoleIO a

namespace ConsoleDo
    export
    (>>=) : Command a -> (a -> Inf (ConsoleIO b)) -> ConsoleIO b
    (>>=) = Do

    export
    (>>) : Command () -> Inf (ConsoleIO b) -> ConsoleIO b
    (>>) = Seq

data Input =
    Answer Int
    | QuitCmd

readInput : (prompt : String) -> Command Input
readInput prompt = do
    PutStr prompt
    answer <- GetLine
    if toLower answer == "quit"
        then Pure QuitCmd
        else Pure (Answer (cast answer))

data Score : Type where
    MkScore : (wins : Nat) -> (losses : Nat) -> Score

Show Score where
    show (MkScore wins losses) = show wins ++ " / " ++ show (wins + losses)

initScore : Score
initScore = MkScore 0 0

won : Score -> Score
won (MkScore wins losses) = MkScore (wins + 1) losses

lost : Score -> Score
lost (MkScore wins losses) = MkScore (wins) (losses + 1)

mutual
    correct : Stream Int -> (score : Score) -> ConsoleIO Score
    correct nums score = do
        PutStr "Correct!\n"
        quiz nums (won score)

    wrong : Stream Int -> Int -> (score : Score) -> ConsoleIO Score
    wrong nums ans score = do
        PutStr ("Wrong, the answer is " ++ show ans ++ "\n")
        quiz nums (lost score)

    quiz : Stream Int -> (score : Score) -> ConsoleIO Score
    quiz (num1 :: num2 :: nums) score = do
        PutStr ("Score so far: " ++ show score ++ "\n")
        input <- readInput (show num1 ++ " * " ++ show num2 ++ "? ")
        case input of
            Answer answer =>
                if answer == num1 * num2
                    then correct nums score
                    else wrong nums (num1 * num2) score
            QuitCmd => Quit score

-- Didn't figure out shiftR from Data.Bits
shiftRight : Int -> Nat -> Int
shiftRight n Z = n
shiftRight n (S k) = shiftRight (n `div` 2) k

randoms : Int -> Stream Int
randoms seed = let seed' = 1664525 * seed + 1013904223 in (seed' `shiftRight` 2) :: randoms seed'

arithInputs : Int -> Stream Int
arithInputs seed = map bound (randoms seed)
  where
    bound : Int -> Int
    bound x with (divides x 12)
        bound ((12 * div) + rem) | (DivBy div rem prf) = abs rem + 1

data Phuel = Dry | More (Lazy Phuel)

partial
forever : Phuel
forever = More forever

partial -- Because runCommand is partial
run : Phuel -> ConsoleIO a -> IO (Maybe a)
run fuel (Quit val) = pure (Just val)
run (More fuel) (Do c f) = do
    res <- runCommand c
    run fuel (f res)
run (More fuel) (Seq c d) = do
    runCommand c
    run fuel d
run Dry p = pure Nothing

partial
runQuiz : IO ()
runQuiz = do
    seed <- time
    Just score <- run forever (quiz (arithInputs (fromInteger seed)) initScore) | Nothing => putStrLn "Ran out of fuel"
    putStrLn ("Final score: " ++ show score)

data ShellCommand =
    Cat String
    | Copy String String
    | Exit

parseCommand : String -> Maybe ShellCommand
parseCommand input =
    case werds input of
        ["cat", file] => Just $ Cat file
        ["copy", from, to] => Just $ Copy from to
        ["exit"] => Just Exit
        _ => Nothing

mutual
    shell : ConsoleIO ()
    shell = do
        PutStr "> "
        input <- GetLine
        case parseCommand input of
            Just (Cat filename) => cat filename
            Just (Copy from to) => copy from to
            Just Exit => Quit ()
            Nothing => do
                PutStr $ "Command not recognized" ++ "\n"
                shell

    cat : String -> ConsoleIO ()
    cat filename = do
        Right content <- ReadFile filename | Left error => logError error
        PutStr content
        shell

    copy : String -> String -> ConsoleIO ()
    copy from to = do
        Right content <- ReadFile from | Left error => logError error
        Right () <- WriteFile to content | Left error => logError error
        shell

    logError : FileError -> ConsoleIO ()
    logError error = do
        PutStr $ show error ++ "\n"
        shell

partial
runShell : IO ()
runShell = do
    Just () <- run forever shell | Nothing => putStrLn "Ran out of fuel"
    pure ()
