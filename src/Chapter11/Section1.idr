module Chapter11.Section1

total
every_other : (xs : Stream e) -> Stream e
every_other (x :: y :: xs) = y :: every_other xs

data InfList : Type -> Type where
    (::) : (value : e) -> Inf (InfList e) -> InfList e

%name InfList xs, ys, zs

Functor InfList where
    map f (x :: xs) = f x :: map f xs

total
countFrom : Integer -> InfList Integer
countFrom x = x :: countFrom (x + 1)

total
getPrefix : Nat -> InfList a -> List a
getPrefix Z x = []
getPrefix (S k) (x :: xs) = x :: getPrefix k xs

data Face = Heads | Tails

total
getFace : Int -> Face
getFace n =
    if mod n 2 == 0
        then Heads
        else Tails

total
coinFlips : (count : Nat) -> Stream Int -> List Face
coinFlips Z (x :: xs) = [getFace x]
coinFlips (S k) (x :: xs) = getFace x :: coinFlips k xs

-- Didn't figure out shiftR from Data.Bits
total
shiftRight : Int -> Nat -> Int
shiftRight n Z = n
shiftRight n (S k) = shiftRight (n `div` 2) k

total
randoms : Int -> Stream Int
randoms seed = let seed' = 1664525 * seed + 1013904223 in (seed' `shiftRight` 2) :: randoms seed'

total
square_root_approx : (number : Double) -> (approx : Double) -> Stream Double
square_root_approx number approx = approx :: square_root_approx number next
  where
    next : Double
    next = (approx + (number/approx))/2

total
square_root_bound : (max : Nat) -> (number : Double) -> (bound : Double) -> (approxs : Stream Double) -> Double
square_root_bound Z number bound (x :: xs) = x
square_root_bound (S k) number bound (x :: xs) =
    if abs(number - x * x) < bound
        then x
        else square_root_bound k number bound xs

total
square_root : (number : Double) -> Double
square_root number = square_root_bound 100 number 0.00000000001 (square_root_approx number number)
