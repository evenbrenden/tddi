module Chapter11.Werds

import Data.SnocList

-- Because (this version of) Data.String.words is partial (for some reason)
-- https://github.com/idris-lang/Idris2/blob/main/libs/base/Data/String.idr
export
werds : String -> List String
werds s = map pack (werds' (unpack s) [<] [<])
  where
    werdsHelper : SnocList Char -> SnocList (List Char) -> SnocList (List Char)
    werdsHelper [<] css = css
    werdsHelper sc  css = css :< (sc <>> Nil)

    werds' :  List Char -> SnocList Char -> SnocList (List Char) -> List (List Char)
    werds' (c :: cs) sc css =
        if isSpace c
            then werds' cs [<] (werdsHelper sc css)
            else werds' cs (sc :< c) css
    werds' [] sc css = werdsHelper sc css <>> Nil
