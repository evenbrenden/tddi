module Chapter11.Section2

%default total

data InfIO : Type where
    Do : IO a -> (a -> Inf InfIO) -> InfIO
    Seq : IO () -> Inf InfIO -> InfIO

(>>=) : IO a -> (a -> Inf InfIO) -> InfIO
(>>=) = Do

(>>) :  IO () -> Inf InfIO -> InfIO
(>>) = Seq

data Fuel = Dry | More (Lazy Fuel)

run : Fuel -> InfIO -> IO ()
run (More fuel) (Do c f) =
    do
        res <- c
        run fuel (f res)
run (More fuel) (Seq c d) =
    do
        c
        run fuel d
run Dry p = putStrLn "Out of fuel"

partial
forever : Fuel
forever = More forever

totalREPL : (prompt : String) -> (action : String -> String) -> InfIO
totalREPL prompt f = do
    putStr prompt
    input <- getLine
    putStr (f input)
    totalREPL prompt f
