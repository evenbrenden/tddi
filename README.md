# tddi

My solutions to the exercises from [Type-Driven Development with Idris by Edwin Brady](https://www.manning.com/books/type-driven-development-with-idris) with Idris 2.

## Build

```
nix build
```

## Develop

```
nix shell
rlwrap idris2 -p contrib
```
