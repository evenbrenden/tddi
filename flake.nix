{
  description = "TDDI";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    idris2-pkgs.url = "github:evenbrenden/idris2-pkgs";
    nixpkgs.follows = "idris2-pkgs/nixpkgs";
  };

  outputs = { self, nixpkgs, idris2-pkgs, flake-utils }:
    flake-utils.lib.eachSystem [ "x86_64-darwin" "x86_64-linux" "i686-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ idris2-pkgs.overlay ];
        };
        inherit (pkgs.idris2-pkgs._builders) idrisPackage devEnv;
        tddi = idrisPackage ./. { };
      in {
        defaultPackage = tddi;
        packages = { inherit tddi; };
        devShell = pkgs.mkShell { buildInputs = with pkgs; [ (devEnv tddi) rlwrap ]; };
      });
}
